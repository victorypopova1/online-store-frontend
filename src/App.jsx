import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';

import Header from './layout/Header'
import ListBrands from './pages/task/ListBrands'
import AddBrand from './pages/task/AddBrand'
import BrandData from './pages/task/BrandData'

import Login from "./pages/authorization/Login";
import Register from "./pages/authorization/Register";
import Profile from "./pages/authorization/Profile";
import { connect } from "react-redux";

function App({ user }) {
    return (
        <div>
            <BrowserRouter>
                <Header />
                <Routes>
                    <Route path='/listBrands' element={<ListBrands/>} />
                    <Route path='/addBrand' element={<AddBrand/>} />
                    <Route path="/brand/:id" element={<BrandData/>}/>
                    <Route path="/login" element={<Login/>} />
                    <Route path="/register" element={<Register/>} />
                    <Route path="/profile" element={<Profile/>} />
                </Routes>
            </BrowserRouter>
        </div>
    );
}

function mapStateToProps(state) {
    const { user } = state.auth;
    return {
        user
    };
}

export default connect(mapStateToProps)(App);