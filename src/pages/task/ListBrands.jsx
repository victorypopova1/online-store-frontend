import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import http from "../../http-common";

function ListBrands() {
    const [brands, setBrands] = useState([]);

    useEffect(() => {
        http
            .get("/brands/")
            .then(response => {
                setBrands(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }, []);

    return (
        <div className="container-md mt-3">
            <div className="col-sm-6">
                <Link to="/addBrand" className="btn btn-success">Добавить бренд</Link>
                {brands.length ? (
                    <ul className="list-group mt-3">
                        {brands.map((brand) => (
                            <Link to={`/brand/${brand.id}`} key={brand.id} className="list-group-item list-group-item-action">
                                {brand.name}
                            </Link>
                        ))}
                    </ul>
                ) : (
                    <div className="alert alert-info mt-3">Подождите, идёт загрузка данных</div>
                )}
            </div>
        </div>
    );
}

export default ListBrands;