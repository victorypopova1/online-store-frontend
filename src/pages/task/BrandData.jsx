import React, { useState, useEffect }  from 'react';

import http from "../../http-common";

import { Navigate, useParams } from 'react-router-dom';

function BrandData() {

    const { id } = useParams();
    const [brand, setBrand] = useState({
        id: id,
        name: ""
    });

    const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        if (!id) {
            return;
        }

        function getBrand() {
            http.get("/brand/" + id)
                .then(response => {
                    setBrand(prevBrand => ({
                        ...prevBrand,
                        name: response.data.name
                    }));
                })
                .catch(e => {
                    console.log(e);
                });
        }

        getBrand();
    }, [id]);


    function handleChange(event) {
        setBrand({
            ...brand,
            name: event.target.value
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        var data = {
            name: brand.name
        };
        http
            .put("/brand/update/" + brand.id + "/", data)
            .then(() => {
                setSubmitted(true);
            })
            .catch(e => {
                console.log(e);
            });
    }

    function deleteBrand() {
        http
            .delete("/brand/delete/" + brand.id  + "/")
            .then(() => {
                setSubmitted(true);
            })
            .catch(e => {
                console.log(e);
            });
    }

    return (
        !submitted
            ?
        <div className="container-md mt-3">
            <div className="col-sm-6">
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <input
                            type="text"
                            name="name"
                            value={brand.name}
                            placeholder="Наименование бренда"
                            onChange={handleChange}
                            className="form-control"
                        />
                    </div>
                    <div className="btn-group mt-2">
                        <button type="submit" className="btn btn-success rounded">Обновить</button>
                        <button className="btn btn-danger  mx-1 rounded" onClick={deleteBrand}>Удалить</button>
                    </div>
                </form>

            </div>
        </div>
        : <Navigate to="/listBrands" />
    )
}

export default BrandData;